let request = fetch("https://project-1-api.herokuapp.com/showdates/?api_key=Jack");
var showDateArray= [];

request.then((response)=>{
    return response.json();
})
.then((json)=> {
    getDates(json);
});

function getDates(arrayVar){
    console.log(arrayVar);
    looper(arrayVar);
}

function looper(randomArrayVar){
    for(var x = 0;x<randomArrayVar.length; x++){
        objectToDiv(randomArrayVar[x]);
    }

var showDatesObject = {};
showDatesObject={date:textDate,place:textPlace,location:textLocation};

function objectToDiv(showDatesObject){
    var postedDates= document.getElementById('dateBoxes');
    let wrapper = document.createElement('div');

    //wraps stacked items
    let subWrapper = document.createElement('div');

    let nodeA =  document.createElement('div');
    let nodeB =  document.createElement('div');
    let nodeC =  document.createElement('div');
    //trying to add in button
    let button = document.createElement('div');

    //setting the class name for formatting css
    wrapper.classList.add("date","dateBoxes");
    nodeA.classList.add("showDate","bold","block","stacked");
    nodeB.classList.add("block","stacked");
    nodeC.classList.add("block");
    button.classList.add("getTickets");
    
    nodeA.innerText = showDatesObject.date;
    nodeB.innerText = showDatesObject.place;
    nodeC.innerText = showDatesObject.location;
    button.innerText = "Get Tickets";

    subWrapper.appendChild(nodeA);
    subWrapper.appendChild(nodeB);
    
    wrapper.appendChild(subWrapper);
    wrapper.appendChild(nodeC);
    wrapper.appendChild(button);

    postedDates.appendChild(wrapper);
}

}